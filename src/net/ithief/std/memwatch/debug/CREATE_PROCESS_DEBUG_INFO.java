package net.ithief.std.memwatch.debug;

import java.util.Arrays;
import java.util.List;

import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import com.sun.jna.platform.win32.WinNT.HANDLE;
import com.sun.jna.ptr.LongByReference;

public class CREATE_PROCESS_DEBUG_INFO extends Structure {
	public HANDLE hFile;
	public HANDLE hProcess;
	public HANDLE hThread;
	public LongByReference lpBaseOfImage;
	public int dwDebugInfoFileOffset;
	public int nDebugInfoSize;
	public LongByReference lpThreadLocalBase;
	public Pointer lpStartAddress;
	public LongByReference lpImageName;
	public short fUnicode;

	@Override
	protected List<?> getFieldOrder() {
		return Arrays.asList("hFile", "hProcess", "hThread", "lpBaseOfImage", "dwDebugInfoFileOffset", "nDebugInfoSize", "lpThreadLocalBase", "lpStartAddress", "lpImageName", "fUnicode");
	}

}
