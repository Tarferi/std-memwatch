package net.ithief.std.memwatch.debug;

import java.util.Arrays;
import java.util.List;

import com.sun.jna.Pointer;
import com.sun.jna.Structure;

public class EXCEPTION_DEBUG_INFO extends Structure {
	public EXCEPTION_RECORD ExceptionRecord;
	public int dwFirstChance;

	public void fill(Pointer p) {
		ExceptionRecord.fill(p);
		p=p.getPointer(ExceptionRecord.size());
		dwFirstChance=p.getInt(0);
	}
	
	@Override
	protected List<?> getFieldOrder() {
		return Arrays.asList("ExceptionRecord", "dwFirstChance");
	}

}
