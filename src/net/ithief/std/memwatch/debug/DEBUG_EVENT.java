package net.ithief.std.memwatch.debug;

import java.util.Arrays;
import java.util.List;

import com.sun.jna.Pointer;
import com.sun.jna.Structure;

public class DEBUG_EVENT extends Structure {
	public int dwDebugEventCode;
	public int dwProcessId;
	public int dwThreadId;
	public DebugEventUnion u;

	public void fill(Pointer p) {
		dwDebugEventCode = p.getInt(0);
		dwProcessId = p.getInt(4);
		dwThreadId = p.getInt(8);
		//System.out.println(dwDebugEventCode + ":" + dwProcessId + ":" + dwThreadId);
		u.fill(p.getPointer(12));
	}

	public static final int EXCEPTION_DEBUG_EVENT = 1;
	public static final int CREATE_THREAD_DEBUG_EVENT = 2;
	public static final int CREATE_PROCESS_DEBUG_EVENT = 3;
	public static final int EXIT_THREAD_DEBUG_EVENT = 4;
	public static final int EXIT_PROCESS_DEBUG_EVENT = 5;
	public static final int LOAD_DLL_DEBUG_EVENT = 6;
	public static final int UNLOAD_DLL_DEBUG_EVENT = 7;
	public static final int OUTPUT_DEBUG_STRING_EVENT = 8;
	public static final int RIP_EVENT = 9;

	public DEBUG_EVENT() {
		super();
	}

	@Override
	protected List<?> getFieldOrder() {
		return Arrays.asList("dwDebugEventCode", "dwProcessId", "dwThreadId", "u");
	}

}
