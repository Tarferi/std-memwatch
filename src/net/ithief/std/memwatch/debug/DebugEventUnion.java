package net.ithief.std.memwatch.debug;

import com.sun.jna.Pointer;
import com.sun.jna.Union;

public class DebugEventUnion extends Union {

	public void fill(Pointer p) {
	}

	public EXCEPTION_DEBUG_INFO Exception;
	public CREATE_THREAD_DEBUG_INFO CreateThread;
	public CREATE_PROCESS_DEBUG_INFO CreateProcessInfo;
	public EXIT_THREAD_DEBUG_INFO ExitThread;
	public EXIT_PROCESS_DEBUG_INFO ExitProcess;
	public LOAD_DLL_DEBUG_INFO LoadDll;
	public UNLOAD_DLL_DEBUG_INFO UnloadDll;
	public OUTPUT_DEBUG_STRING_INFO DebugString;
	public RIP_INFO RipInfo;
}
