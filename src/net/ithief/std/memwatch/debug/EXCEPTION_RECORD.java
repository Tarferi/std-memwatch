package net.ithief.std.memwatch.debug;

import java.util.Arrays;
import java.util.List;

import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import com.sun.jna.ptr.LongByReference;

public class EXCEPTION_RECORD extends Structure {

	public int ExceptionCode;
	public int ExceptionFlags;
	public Pointer ExceptionRecord = this.getPointer();
	public Pointer ExceptionAddress;
	public int NumberParameters;
	public LongByReference ExceptionInformation;

	public void fill(Pointer p) {
		ExceptionCode = p.getInt(0);
		ExceptionFlags = p.getInt(4);
		ExceptionRecord = p.getPointer(8);
		p = p.getPointer(8 + Pointer.SIZE);
		ExceptionAddress = p.getPointer(0);
		p = p.getPointer(Pointer.SIZE);
		NumberParameters = p.getInt(0);
		ExceptionInformation = new LongByReference(p.getLong(4));
	}

	@Override
	protected List<?> getFieldOrder() {
		return Arrays.asList("ExceptionCode", "ExceptionFlags", "ExceptionRecord", "ExceptionAddress", "NumberParameters", "ExceptionInformation");
	}

}
