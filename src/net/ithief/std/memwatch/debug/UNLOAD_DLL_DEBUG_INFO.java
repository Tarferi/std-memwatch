package net.ithief.std.memwatch.debug;

import java.util.Arrays;
import java.util.List;

import com.sun.jna.Structure;
import com.sun.jna.ptr.LongByReference;

public class UNLOAD_DLL_DEBUG_INFO extends Structure {

	public LongByReference lpBaseOfDll;
	
	@Override
	protected List<?> getFieldOrder() {
		return Arrays.asList("lpBaseOfDll");
	}

}
