package net.ithief.std.memwatch.debug;

import java.util.Arrays;
import java.util.List;

import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import com.sun.jna.platform.win32.WinNT.HANDLE;
import com.sun.jna.ptr.LongByReference;

public class CREATE_THREAD_DEBUG_INFO extends Structure {

	public HANDLE hThread;
	public LongByReference lpThreadLocalBase;
	public Pointer lpStartAddress;

	@Override
	protected List<?> getFieldOrder() {
		return Arrays.asList("hThread", "lpThreadLocalBase", "lpStartAddress");
	}

}
