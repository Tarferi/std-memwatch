package net.ithief.std.memwatch.debug;

import java.util.Arrays;
import java.util.List;

import com.sun.jna.Structure;

public class RIP_INFO extends Structure {

	public int dwError;
	public int dwType;

	@Override
	protected List<?> getFieldOrder() {
		return Arrays.asList("dwError", "dwType");
	}

}
