package net.ithief.std.memwatch.debug;

import java.util.Arrays;
import java.util.List;

import com.sun.jna.Structure;
import com.sun.jna.ptr.LongByReference;

public class OUTPUT_DEBUG_STRING_INFO extends Structure {
	public LongByReference lpDebugStringData;
	public short fUnicode;
	public short nDebugStringLength;

	@Override
	protected List<?> getFieldOrder() {
		return Arrays.asList("lpDebugStringData", "fUnicode", "nDebugStringLength");
	}

}
