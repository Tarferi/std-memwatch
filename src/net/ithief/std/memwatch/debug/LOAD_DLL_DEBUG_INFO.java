package net.ithief.std.memwatch.debug;

import java.util.Arrays;
import java.util.List;

import com.sun.jna.Structure;
import com.sun.jna.platform.win32.WinNT.HANDLE;
import com.sun.jna.ptr.LongByReference;

public class LOAD_DLL_DEBUG_INFO extends Structure {
	public HANDLE hFile;
	public LongByReference lpBaseOfDll;
	public int dwDebugInfoFileOffset;
	public int nDebugInfoSize;
	public LongByReference lpImageName;
	public short fUnicode;

	@Override
	protected List<?> getFieldOrder() {
		return Arrays.asList("hFile", "lpBaseOfDll", "dwDebugInfoFileOffset", "nDebugInfoSize", "lpImageName", "fUnicode");
	}

}
