package net.ithief.std.memwatch.debug;

import java.util.Arrays;
import java.util.List;

import com.sun.jna.Structure;

public class EXIT_PROCESS_DEBUG_INFO extends Structure {

	public int dwExitCode;
	
	@Override
	protected List<?> getFieldOrder() {
		return Arrays.asList("dwExitCode");
	}

}
