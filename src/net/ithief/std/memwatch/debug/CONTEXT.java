package net.ithief.std.memwatch.debug;

import java.util.Arrays;
import java.util.List;

import com.sun.jna.Structure;

public class CONTEXT extends Structure {

	@Override
	protected List<?> getFieldOrder() {
		return Arrays.asList("Dr0", "Dr1", "Dr2", "Dr3", "Dr6", "Dr7", "SegGs", "SegFs", "SegEs", "SegDs", "Edi", "Esi", "Ebx", "Edx", "Ecx", "Eax", "Ebp", "Eip", "SegCs", "EFlags", "Esp", "SegSs", "ExtendedRegisters");
	}

	private static final int MAXIMUM_SUPPORTED_EXTENSION = 512;
	public int Dr0;
	public int Dr1;
	public int Dr2;
	public int Dr3;
	public int Dr6;
	public int Dr7;

	public int SegGs;
	public int SegFs;
	public int SegEs;
	public int SegDs;

	public int Edi;
	public int Esi;
	public int Ebx;
	public int Edx;
	public int Ecx;
	public int Eax;

	public int Ebp;
	public int Eip;
	public int SegCs;
	public int EFlags;
	public int Esp;
	public int SegSs;

	byte[] ExtendedRegisters = new byte[MAXIMUM_SUPPORTED_EXTENSION];

}
