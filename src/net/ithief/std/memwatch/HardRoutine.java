package net.ithief.std.memwatch;

import net.ithief.std.memwatch.exceptions.NoSuchProcessException;

public class HardRoutine extends Routine {

	public HardRoutine(RoutineExecutor r, Long address, CoreMemory mem) throws NoSuchProcessException {
		super(r, address, mem);
	}

	@Override
	public void restore() throws NoSuchProcessException {
	}

}
