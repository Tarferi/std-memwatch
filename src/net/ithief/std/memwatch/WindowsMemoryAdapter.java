package net.ithief.std.memwatch;

import java.util.HashMap;
import java.util.Map;

import net.ithief.std.memwatch.exceptions.NoSuchProcessException;

import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.platform.win32.Tlhelp32.PROCESSENTRY32;
import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.platform.win32.WinNT.HANDLE;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.win32.StdCallLibrary;
import com.sun.jna.win32.W32APIOptions;

public abstract class WindowsMemoryAdapter implements CoreMemory {
	private static final Kernel32 kernel32 = (Kernel32) Native.loadLibrary(Kernel32.class, W32APIOptions.UNICODE_OPTIONS);
	private static final NativeKernel nativeKernel32 = (NativeKernel) Native.loadLibrary("kernel32", NativeKernel.class);

	public interface NativeKernel extends StdCallLibrary {
		boolean WriteProcessMemory(HANDLE hProcess, int lpBaseAddress, Pointer lpBuffer, int nSize, IntByReference lpNumberOfBytesWritten);

		boolean ReadProcessMemory(HANDLE hProcess, int inBaseAddress, Pointer outputBuffer, int nSize, IntByReference outNumberOfBytesRead);

		HANDLE OpenProcess(int desired, boolean inherit, int pid);

		HANDLE CreateToolhelp32Snapshot(int a, int b);

		boolean DebugActiveProcess(DWORD pid);

		boolean DebugActiveProcessStop(DWORD pid);
		
		boolean DebugSetProcessKillOnExit(boolean kill);

		void GetThreadContext(HANDLE thread, Pointer pointer);

		boolean WaitForDebugEvent(Pointer p, int maxValue);

		void ContinueDebugEvent(int pid, int thread, int i);

	}

	private final DWORD pid;

	protected NativeKernel getNativeKernel() {
		return nativeKernel32;
	}

	protected Kernel32 getKernel() {
		return kernel32;
	}

	// private long pid;
	private HANDLE process;

	public WindowsMemoryAdapter(String windowName) throws NoSuchProcessException {
		this.pid = new DWORD(FindProcessId(windowName));
		process = nativeKernel32.OpenProcess(0x0010 | 0x0020 | 0x0008 | 0x0400 | 0x0200, true, pid.intValue());
	}

	private synchronized byte[] read(long address, Memory m, int size) {
		nativeKernel32.ReadProcessMemory(process, (int) address, m, (int) size, zerodefR);
		return m.getByteArray(0, size);
	}

	private IntByReference zerodefW = new IntByReference(0);
	private IntByReference zerodefR = new IntByReference(0);

	private synchronized void write(long address, Memory m, byte[] data) {
		m.write(0, data, 0, data.length);
		nativeKernel32.WriteProcessMemory(process, (int) address, m, data.length, zerodefW);
	}

	static long FindProcessId(String processName) throws NoSuchProcessException {
		HANDLE h = nativeKernel32.CreateToolhelp32Snapshot(0x00000002, 0);
		if (h != null) {
			PROCESSENTRY32 struct = new PROCESSENTRY32();
			if (!kernel32.Process32First(h, struct)) {
				System.out.println("Fail: " + kernel32.GetLastError());
				kernel32.CloseHandle(h);
			} else {
				do {
					String procname = Native.toString(struct.szExeFile);
					if (procname.equals(processName)) {
						kernel32.CloseHandle(h);
						return struct.th32ProcessID.longValue();
					}
				} while (kernel32.Process32Next(h, struct));
				kernel32.CloseHandle(h);
			}
		}
		throw new NoSuchProcessException();
	}

	private final Memory byteMemR = new Memory(1);
	private final Memory shortMemR = new Memory(2);
	private final Memory intMemR = new Memory(4);
	private final Memory strMemR = new Memory(maxStringLength);

	private final Memory byteMemW = new Memory(1);
	private final Memory shortMemW = new Memory(2);
	private final Memory intMemW = new Memory(4);
	private final Memory strMemW = new Memory(maxStringLength);

	@Override
	public synchronized short readShort(long address) throws NoSuchProcessException {
		byte[] data = read(address, shortMemR, 2);
		return (short) (((data[1] & 0xff) << 8) | (data[0] & 0xff));
	}

	@Override
	public synchronized int readInt(long address) throws NoSuchProcessException {
		byte[] data = read(address, intMemR, 4);
		return (((data[3] & 0xff) << 24) | ((data[2] & 0xff) << 16) | ((data[1] & 0xff) << 8) | (data[0] & 0xff));
	}

	@Override
	public synchronized byte readByte(long address) throws NoSuchProcessException {
		byte[] data = read(address, byteMemR, 1);
		return data[0];
	}

	private static final int maxStringLength = 128;

	@Override
	public synchronized String readString(long address, boolean unicode) throws NoSuchProcessException {
		byte[] buffer = read(address, strMemR, maxStringLength);
		StringBuilder sb = new StringBuilder();
		if (unicode) {
			for (int i = 0; i < buffer.length; i += 2) {
				byte b = buffer[i];
				if (b == 0) {
					break;
				} else {
					sb.append((char) b);
				}
			}
		} else {
			for (byte b : buffer) {
				if (b == 0) {
					break;
				} else {
					sb.append((char) b);
				}
			}
		}
		return sb.toString();
	}

	public DWORD getPID() {
		return pid;
	}

	@Override
	public synchronized void writeByte(long address, byte b) throws NoSuchProcessException {
		write(address, byteMemW, new byte[] { b });
	}

	@Override
	public synchronized void writeShort(long address, short s) throws NoSuchProcessException {
		write(address, shortMemW, new byte[] { (byte) ((s >> 8) & 0xff), (byte) (s & 0xff) });
	}

	@Override
	public synchronized void writeInt(long address, int i) throws NoSuchProcessException {
		write(address, intMemW, new byte[] { (byte) ((i >> 0) & 0xff), (byte) ((i >> 8) & 0xff), (byte) ((i >> 16) & 0xff), (byte) ((i >> 24) & 0xff), });
	}

	@Override
	public synchronized void writeString(long address, String s, boolean unicode) throws NoSuchProcessException {
		if (s.length() > strMemW.size()) {
			s = s.substring(0, (int) strMemW.size());
		}
		byte[] data = s.getBytes();
		if (unicode) {
			byte[] origin = data;
			data = new byte[origin.length * 2];
			for (int i = 0; i < data.length; i++) {
				data[i * 2] = origin[i];
				data[(i * 2) + 1] = 0;
			}
		}
		write(address, strMemW, s.getBytes());
		writeByte(address + s.getBytes().length, (byte) 0);
	}

	@Override
	public synchronized void readByteArray(long address, byte[] originalInstruction) throws NoSuchProcessException {
		Memory mem = getMem(originalInstruction);
		read(address, mem, originalInstruction.length);
		byte[] read = mem.getByteArray(0, (int) mem.size());
		System.arraycopy(read, 0, originalInstruction, 0, originalInstruction.length);
	}

	private final Map<Integer, Memory> nativeMemMap = new HashMap<>();

	private synchronized Memory getMem(byte[] originalInstruction) {
		Memory mem = nativeMemMap.get(originalInstruction.length);
		if (mem == null) {
			mem = new Memory(originalInstruction.length);
			nativeMemMap.put(originalInstruction.length, mem);
		}
		return mem;
	}

	@Override
	public synchronized void writeByteArray(long address, byte[] originalInstruction) throws NoSuchProcessException {
		write(address, getMem(originalInstruction), originalInstruction);
	}

}