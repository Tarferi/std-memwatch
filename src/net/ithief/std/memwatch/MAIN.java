package net.ithief.std.memwatch;

import net.ithief.std.memwatch.exceptions.NoSuchProcessException;

public class MAIN {

	public static void main(String[] args) {
		try {
			ExtendedKernelAdapter mem = new ExtendedKernelAdapter("chrome.exe");
			mem.getNativeKernel();
		} catch (NoSuchProcessException e) {
			e.printStackTrace();
		}
	}

}
