package net.ithief.std.memwatch;

import net.ithief.std.memwatch.debug.CONTEXT;
import net.ithief.std.memwatch.exceptions.NoSuchProcessException;
import net.ithief.std.memwatch.exceptions.NoSuchThreadException;

public interface CoreMemory {

	public short readShort(long address) throws NoSuchProcessException;

	public int readInt(long address) throws NoSuchProcessException;

	public byte readByte(long address) throws NoSuchProcessException;

	public String readString(long address, boolean unicode) throws NoSuchProcessException;

	public void writeByte(long address, byte b) throws NoSuchProcessException;

	public void writeShort(long address, short s) throws NoSuchProcessException;

	public void writeInt(long address, int i) throws NoSuchProcessException;

	public void writeString(long address, String s, boolean unicode) throws NoSuchProcessException;

	public CONTEXT getContext(ThreadHandle Thread) throws NoSuchProcessException, NoSuchThreadException;

	public ThreadHandle getThread(int ThreadID) throws NoSuchThreadException;

	public void closeThread(ThreadHandle handle);

	public void readByteArray(long address, byte[] originalInstruction) throws NoSuchProcessException;

	public void writeByteArray(long address, byte[] originalInstruction) throws NoSuchProcessException;
}
