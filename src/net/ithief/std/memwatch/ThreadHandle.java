package net.ithief.std.memwatch;

import com.sun.jna.platform.win32.WinNT.HANDLE;

public class ThreadHandle {

	private HANDLE h;

	public ThreadHandle(HANDLE h) {
		this.h=h;
	}
	
	protected HANDLE getHandle() {
		return h;
	}
	
}
