package net.ithief.std.memwatch;

import net.ithief.std.memwatch.debug.CONTEXT;
import net.ithief.std.memwatch.debug.DEBUG_EVENT;
import net.ithief.std.memwatch.exceptions.AttachmentFailedException;
import net.ithief.std.memwatch.exceptions.NoSuchProcessException;
import net.ithief.std.memwatch.exceptions.NoSuchThreadException;

import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.WinNT.HANDLE;

public class ExtendedKernelAdapter extends WindowsMemoryAdapter {

	public ExtendedKernelAdapter(String windowName) throws NoSuchProcessException {
		super(windowName);
	}

	private final RoutineManager man = new RoutineManager();

	private Thread thr;

	public void addBreakpoint(long address, RoutineExecutor run) throws AttachmentFailedException, NoSuchProcessException {
		man.addRoutine(address, new Routine(run, address, this));
	}

	public void nopBreakpoint(long address) throws AttachmentFailedException, NoSuchProcessException {
		man.nopRoutine(address, this);
	}

	public void addHardBreakpoint(long address, RoutineExecutor run) throws AttachmentFailedException, NoSuchProcessException {
		man.addRoutine(address, new HardRoutine(run, address, this));
	}

	public void addBreakpointRedirect(long address1, long addres2) {
		man.redirectRoutie(address1, addres2);
	}

	public void attach(final FailHandler h) {
		thr = new Thread() {
			@Override
			public void run() {
				setName("Handler thread");
				try {
					attachSelf();
					handle();
				} catch (AttachmentFailedException e1) {
					if (h != null) {
						h.fail(e1);
					}
				} finally {
					man.restoreAllRoutines();
				}
				try {
					detachSelf();
				} catch (AttachmentFailedException e2) {
					if (h != null) {
						h.fail(e2);
					}
				}
			}
		};
		thr.start();
	}

	public void detach() {
		try {
			detachSelf();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	private void attachSelf() throws AttachmentFailedException {
		NativeKernel kernel = super.getNativeKernel();
		if (!kernel.DebugActiveProcess(super.getPID())) {
			throw new AttachmentFailedException();
		}
	}

	private boolean detached = false;

	private void detachSelf() throws AttachmentFailedException {
		if (!detached) {
			detached = true;
			NativeKernel kernel = super.getNativeKernel();
			try {
				man.overrunRoutine();
			} catch (NoSuchProcessException e) {
				throw new AttachmentFailedException();
			}
			if (!kernel.DebugActiveProcessStop(super.getPID())) {
				int lasterr = super.getKernel().GetLastError();
				System.err.println("Last error: " + lasterr);
				throw new AttachmentFailedException();
			}
		}
	}

	public void closeHandle(HANDLE h) {
		super.getKernel().CloseHandle(h);
	}

	public ThreadHandle getThread(int thread) throws NoSuchThreadException {
		System.out.println("Requesting thread");
		HANDLE h = super.getKernel().OpenThread(0x0008 | 0x0040 | 0x0010 | 0x20000, false, thread);
		System.out.println("Thread found: " + h);
		if (h == null) {
			throw new NoSuchThreadException();
		} else {
			return new ThreadHandle(h);
		}
	}

	public CONTEXT getContext(HANDLE Thread) {
		CONTEXT c = new CONTEXT();
		super.getNativeKernel().GetThreadContext(Thread, c.getPointer());
		return c;
	}

	private void handle() throws AttachmentFailedException {
		super.getNativeKernel().DebugSetProcessKillOnExit(false);
		DEBUG_EVENT e = new DEBUG_EVENT();
		Pointer p = e.getPointer();
		int failCount = 0;
		NativeKernel kernel = super.getNativeKernel();
		while (true) {
			try {
				man.overrunRoutine();
			} catch (NoSuchProcessException e1) {
				throw new AttachmentFailedException();
			}
			if (detached) {
				return;
			}
			if (!kernel.WaitForDebugEvent(p, Integer.MAX_VALUE)) {
				if (detached) {
					return;
				}
				System.err.println("Fail");
				failCount++;
				if (failCount > 20) {
					kernel.DebugActiveProcessStop(super.getPID());
					throw new AttachmentFailedException();
				}
			}
			if (detached) {
				return;
			}
			failCount = 0;
			e.fill(p);
			int code = e.dwDebugEventCode;
			int pid = e.dwProcessId;
			int thread = e.dwThreadId;
			if (code == 1) {
				try {
					long addr = p.getLong(20) >> 32;
					man.handleRoutine(addr, thread);
				} catch (NoSuchProcessException e1) {
					e1.printStackTrace();
					kernel.ContinueDebugEvent(pid, thread, 0x00010002);
					throw new AttachmentFailedException();
				}
			}
			if (code == 5) {
				kernel.ContinueDebugEvent(pid, thread, 0x00010002);
				return;
			}
			kernel.ContinueDebugEvent(pid, thread, 0x00010002);
		}
	}

	@Override
	public CONTEXT getContext(ThreadHandle Thread) throws NoSuchProcessException, NoSuchThreadException {
		return getContext(Thread.getHandle());
	}

	@Override
	public void closeThread(ThreadHandle handle) {
		closeHandle(handle.getHandle());
	}
}
