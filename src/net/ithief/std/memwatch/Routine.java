package net.ithief.std.memwatch;

import net.ithief.std.memwatch.exceptions.NoSuchProcessException;

public class Routine {

	private RoutineExecutor r;
	private CoreMemory mem;
	private byte originalInstruction;
	private byte debugInstruction = (byte) 0xCC;
	private Long addr;

	public Routine(RoutineExecutor r, Long address, CoreMemory mem) throws NoSuchProcessException {
		this.r = r;
		this.mem = mem;
		this.addr = address;
		readOriginalInstruction(addr);
	}

	protected void readOriginalInstruction(long address) throws NoSuchProcessException {
		originalInstruction = mem.readByte(address);
	}

	protected void writeOriginalInstruction(long address) throws NoSuchProcessException {
		mem.writeByte(address, originalInstruction);
	}

	protected void writeDebugInstruction(long address) throws NoSuchProcessException {
		mem.writeByte(address, debugInstruction);
	}

	public void overrun() throws NoSuchProcessException {
		writeDebugInstruction(addr);
	}

	public void run(int thread) throws NoSuchProcessException {
		restore();
		try {
			r.execute(addr, mem, thread);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public void restore() throws NoSuchProcessException {
		writeOriginalInstruction(addr);
	}
}
