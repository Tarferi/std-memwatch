package net.ithief.std.memwatch;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import net.ithief.std.memwatch.exceptions.NoSuchProcessException;

public class RoutineManager {
	private final Map<Long, Routine> routines = new HashMap<>();
	private Routine lastRoutine;

	public void addRoutine(Long l, Routine r) throws NoSuchProcessException {
		synchronized (routines) {
			routines.put(l, r);
		}
		// System.out.println("Adding routine " + l);
		r.overrun();
	}

	public void deleteRoutine(Long l) {
		synchronized (routines) {
			routines.remove(l);
		}
	}

	public void handleRoutine(Long l, int thread) throws NoSuchProcessException {
		synchronized (routines) {
			lastRoutine = routines.get(l);
		}
		if (lastRoutine != null) {
			// System.out.println("Handling routine " + l);
			lastRoutine.run(thread);
		} else {
			System.err.println("Unhandled routine at " + l);
		}
	}

	public void overrunRoutine() throws NoSuchProcessException {
		if (lastRoutine != null) {
			lastRoutine.overrun();
			lastRoutine = null;
		}
	}

	public void restoreLastRoutine() throws NoSuchProcessException {
		if (lastRoutine != null) {
			lastRoutine.restore();
			lastRoutine = null;
		}
	}

	public void restoreAllRoutines() {
		synchronized (routines) {
			Set<Entry<Long, Routine>> es = routines.entrySet();
			for (Entry<Long, Routine> e : es) {
				try {
					e.getValue().restore();
				} catch (NoSuchProcessException e1) {
				}
			}
		}
	}

	public void redirectRoutie(long address1, long addres2) {
		synchronized (routines) {
			Routine f = routines.get(address1);
			routines.put(addres2, f);
		}
	}

	public void nopRoutine(long address,CoreMemory mem) throws NoSuchProcessException {
		mem.writeByte(address, (byte) 0x90);
	}
}
