package net.ithief.std.memwatch;

import net.ithief.std.memwatch.exceptions.AttachmentFailedException;

public interface FailHandler {

	public void fail(AttachmentFailedException e);
	
}
