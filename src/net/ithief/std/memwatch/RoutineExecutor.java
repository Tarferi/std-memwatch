package net.ithief.std.memwatch;

public interface RoutineExecutor {

	public void execute(long address, CoreMemory memory, int ThreadID);
	
}
